# project goal
the current masscarde [implementation](https://gitlab.com/masscarade/legacy.git) is executed locally and as a 
monolith. adding new features, especially in a productive environment is not straight forward.

As an alternative, masscarade will be re-implemented as a set of microservices, which communicate with a REST API.
This project serves as a playground to get familar with the technology in an organized way. The starting point is
a simple service, which stores file. through a REST api new files can be added, existing files can be retrieved 
and all available files can be listed. furthermore a quick HTML introduction served through REST whould be available.

# the API
as a matter of laziness some examples with curl will do for now:

uploading a file:
curl http://localhost:1234/files/blabla/xyz/abc/ima444ge.svg -d @"license.lic"

listing all available files:
curl http://localhost:1234/files

downloading a file:
curl http://localhost:1234/files/blabla/xyz/abc/ima444ge.svg

getting the HTML help:
curl http://localhost:1234/info

# the code
heart of the service is the FileProvider class. it gets instanciated and executed once in the main. based on the settings 
it will instanciate and run a httplistener. the GetContext method of the listener will wait until any HTTP request occurs 
and returns the context, containing request and resonse.

nested in the FileProvider class, is the static class Requests. it contains a dictionary with strings as keys and instances 
of the Request class as values. this is static, because those definitions are the same for each potential instance of the FileProvider class. 
the Request class is a abstract class, defining the base for all requets the service can handle. each request, info for retrieving the HTML 
help, files for doing the file operations and potentially more, needs a class which is derived from the Request class. 

the main task for the Request classes, is to define the URL mapping for the corresponding request and to return workers which handle
the actual request based on the context delivered by the httplistener. in the static constructor of Requests instances of all Request 
derivations are added to the dictionary with the URL mapping as key.

so when a new context is delivered by the httplistener, it is passed to the Requests class, which is then returning a worker which can
handle the request correspondingly. this worker is then executed in a new thread.

# next steps
the basic architecture of the REST API has to be tested more in detail. when those tests are positive, more services will be implemented 
in an analog way.



