﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;

namespace file_provider
{   
    class Program
    {
        /// <summary>
        /// directory where the files known to the service are stored; consider setting with a cli argument
        /// </summary>
        public static string filesDir { get { return "files"; } }

        /// <summary>
        /// directory where the settings are stored; consider setting with cli argument
        /// </summary>
        public static string settingsDir { get { return "settings"; } }

        /// <summary>
        /// name of the main settings file within the settingsDir folder; consider setting with cli argument
        /// </summary>
        public static string settingsFile { get { return "settings.xml"; } }
        public static string settingsFileTotal { get { return settingsDir + @"/" + settingsFile; } }

        /// <summary>
        /// service entry point; args containing cli arguments
        /// </summary>
        /// <param name="args"></param>
        static void Main(string[] args)
        {
            //create directory for the files if not existing
            if (Directory.Exists(filesDir) == false) { Directory.CreateDirectory(filesDir); }

            Settings settings;

            //create directory for settings if not existing
            if (Directory.Exists(settingsDir) == false) { Directory.CreateDirectory(settingsDir); }

            //save sample settings if no settings existing
            if (File.Exists(settingsFileTotal) == false)
            {
                settings = Settings.getSample();

                //xml settings for improving readability by formatting -> newlines, tabs, etc
                XmlWriterSettings xmlSettings = new XmlWriterSettings()
                {
                    Indent = true,
                };

                using (XmlWriter write = XmlWriter.Create(settingsFileTotal, xmlSettings))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Settings));

                    serializer.WriteObject(write, settings);
                }
            }
            //read existing settings
            else
            {
                using (XmlReader reade = XmlReader.Create(settingsFileTotal))
                {
                    DataContractSerializer serializer = new DataContractSerializer(typeof(Settings));

                    settings = (Settings)serializer.ReadObject(reade);
                }
            }

            //create service instance and run it; twoliner for better debugging
            FileProvider provider = new FileProvider(settings);

            try
            {
                provider.run();
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

                //show all inner exceptions on the console
                for (Exception inner = e.InnerException; inner != null; inner = inner.InnerException)
                {
                    Console.WriteLine("inner excpetion:\n {0}", inner.Message);
                }
            }      

            Console.WriteLine("press any key to exit");
            Console.ReadKey();
        }      
    }
}
