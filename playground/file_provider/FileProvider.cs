﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.Threading;
using System.IO;
using System.Runtime.Serialization;
using System.Xml;
using System.Collections;
using System.Text.RegularExpressions;
using System.Web.Script.Serialization;

namespace file_provider
{
    /// <summary>
    /// class containing settings for the program
    /// </summary>
    [DataContract]
    class Settings
    {
        /// <summary>
        /// prefixes used by the http server
        /// </summary>
        [DataMember]
        public List<string> prefixes { get; set; }

        /// <summary>
        /// cretes a instance with sample values; used as reference for setting values
        /// </summary>
        /// <returns></returns>
        public static Settings getSample()
        {
            return new Settings()
            {
                prefixes = new List<string>(new string[]
                {
                        "http://*:1234/",
                        "https://*:1235/",
                }),
            };
        }
    }

    /// <summary>
    /// abstract worker processing request from httplistenercontext; 
    /// </summary>
    abstract class Worker
    {
        /// <summary>
        /// context which is used for the processing
        /// </summary>
        protected HttpListenerContext context;

        /// <summary>
        /// worker has to be instanciated with httplistener context
        /// </summary>
        /// <param name="context"></param>
        public Worker(HttpListenerContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// processes request with context
        /// </summary>
        public abstract void processRequest();
    }

    /// <summary>
    /// base class for all requests that are served by the file_provider service
    /// </summary>
    abstract class Request
    {
        public abstract string urlPath { get; }

        /// <summary>
        /// returns a worker which is handling the request based on the context
        /// </summary>
        /// <param name="context"></param>
        /// <returns></returns>
        public abstract Worker getWorker(HttpListenerContext context);

        public Request() { }
    }

    /// <summary>
    /// service main class
    /// </summary>
    class FileProvider
    {
        /// <summary>
        /// instance listening for http requests
        /// </summary>
        private HttpListener listener;

        /// <summary>
        /// contains the known requests of the file_provider service
        /// </summary>
        private static class Requests
        {
            /// <summary>
            /// dictionary containing the nown requests
            /// </summary>
            private static Dictionary<string, Request> requests;

            /// <summary>
            /// adds the Request instances to the requests collection
            /// </summary>
            static Requests()
            {
                requests = new Dictionary<string, Request>();

                Request request;

                request = new Requests.Files();
                requests.Add(request.urlPath, request);

                request = new Requests.Info();
                requests.Add(request.urlPath, request);
            }

           /// <summary>
           /// identifies which worker applies to the context and returns it
           /// </summary>
           /// <param name="context"></param>
           /// <returns></returns>
            public static Worker getWorker(HttpListenerContext context)
            {
                foreach (string segment in context.Request.Url.Segments)
                {
                    //get only text and letters from the segment
                    string index = Regex.Replace(segment, "[^a-zA-Z0-9]", "");

                    //valid reuest found
                    if (requests.ContainsKey(index) == true)
                    {
                        return requests[index].getWorker(context);
                    }
                }

                //no valid request found
                return null;
            }                   

            /// <summary>
            /// request handling file actions; listing available files, adding a files and returning files
            /// </summary>
            public class Files : Request
            {
                public Files() : base() { }

                public override string urlPath { get { return "files"; } }
              
                /// <summary>
                /// returns the FilesWorker instance handling the request
                /// </summary>
                /// <param name="context"></param>
                /// <returns></returns>
                public override Worker getWorker(HttpListenerContext context)
                {
                    return new FilesWorker(context);
                }

                /// <summary>
                /// handling file requests
                /// </summary>
                class FilesWorker : Worker
                {
                    public FilesWorker(HttpListenerContext context) : base(context) { }

                    /// <summary>
                    /// used to create the JSON strings for the list request
                    /// </summary>
                    public class FileEntry
                    {
                        public string name;
                        public string href;
                    }

                    public override void processRequest()
                    {
                        //used for nicer code
                        HttpListenerRequest request = context.Request;
                        HttpListenerResponse response = context.Response;

                        //think about nicer solution for aquiring urlPath of files
                        string urlPath = new Files().urlPath;
                        //match from AFTER mapped request url to end of string
                        string requestUrl = Regex.Match(request.RawUrl, string.Format(@"(?<={0}/).*", urlPath)).ToString();                        

                        //empty string means no file requested -> list available files
                        if (requestUrl == string.Empty)
                        {
                            response.ContentType = "application/json";

                            List<FileEntry> result = new List<FileEntry>();

                            Console.WriteLine("listing available files");

                            foreach (string str in (Directory.GetFiles(Program.filesDir, "*", SearchOption.AllDirectories)))
                            {
                                result.Add(new FileEntry()
                                {
                                    href = (request.Url.AbsoluteUri + str.Replace(Program.filesDir, "").Replace(@"\", @"/")).Replace(@"//",@"/"),
                                    name = new FileInfo(str).Name,
                                });
                            }

                            JavaScriptSerializer jss = new JavaScriptSerializer();

                            using (MemoryStream stream = new MemoryStream(Encoding.ASCII.GetBytes(jss.Serialize(result))))
                            {
                                stream.CopyTo(response.OutputStream);
                            }
                        }
                        //a path is specified
                        else
                        {
                            //local file path on the server
                            string filePath = Program.filesDir + @"/" + requestUrl;
                            FileInfo fi = new FileInfo(filePath);

                            //respond with content of file if it exists
                            if (fi.Exists == true)
                            {
                                Console.WriteLine("sending file {0}", fi.Name);

                                using (FileStream stream = File.OpenRead(filePath))
                                {
                                    stream.CopyTo(response.OutputStream);
                                }
                            }
                            //only add the file when POST method is used
                            else if (request.HttpMethod == "POST")
                            {                                
                                fi.Directory.Create();

                                Console.WriteLine("adding file {0}", fi.Name);

                                using (FileStream stream = fi.Create())
                                {
                                    request.InputStream.CopyTo(stream);
                                }
                            }
                            //no valid request
                            else
                            {
                                context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                                context.Response.OutputStream.Close();
                                Console.WriteLine("file in path not existing and no file posted {0}", context.Request.RawUrl);
                            }
                        }

                        //close stream to finish request
                        response.OutputStream.Close();
                    }
                }
            }

            /// <summary>
            /// handles the info request
            /// </summary>
            public class Info : Request
            {
                public Info() : base() { }          

                public override string urlPath { get { return @"info"; } }
           
                /// <summary>
                /// returns a instance of InfoWorker handling the request
                /// </summary>
                /// <param name="context"></param>
                /// <returns></returns>
                public override Worker getWorker(HttpListenerContext context)
                {
                    return new InfoWorker(context);
                }

                /// <summary>
                /// handling info requests based on context
                /// </summary>
                class InfoWorker : Worker
                {
                    public InfoWorker(HttpListenerContext context) : base(context) { }

                    /// <summary>
                    /// returns info html deployed with the program
                    /// </summary>
                    public override void processRequest()
                    {
                        HttpListenerRequest request = context.Request;
                        // Obtain a response object.
                        HttpListenerResponse response = context.Response;
                        // Construct a response.

                        response.ContentType = "text/html";
                        using (FileStream stream = File.OpenRead("Info.html"))
                        {
                            stream.CopyTo(response.OutputStream);
                        }
                        
                        response.OutputStream.Close();
                    }
                }
            }
        }

        public Settings settings { get; private set; }

        /// <summary>
        /// settings are mandatory for a new instance
        /// </summary>
        /// <param name="settings"></param>
        public FileProvider(Settings settings) { this.settings = settings; }

        public void run()
        {

            // URI settings.prefixes are required; stored in settings
            // for example "http://contoso.com:8080/index/".
            if (settings.prefixes == null)
            {
                throw new ArgumentException(string.Format("settings.prefixes is null; check settings file {0}", Program.settingsFileTotal));
            }
            else if (settings.prefixes.Count == 0)
            {
                throw new ArgumentException(string.Format("settings.prefixes is empty; check settings file {0}", Program.settingsFileTotal));
            }

            // Create a listener.
            listener = new HttpListener();
            // Add the settings.prefixes.
            foreach (string s in settings.prefixes)
            {
                listener.Prefixes.Add(s);
            }

            listener.Start();

            Console.WriteLine("Listening for requests...");

            do
            {
                //create new thread executing the worker based on context; twoliner for better debugging
                HttpListenerContext context = listener.GetContext();

                //get worker based on context
                Worker worker = Requests.getWorker(context);

                //no corresponding worker found based on request; continue polling
                if (worker == null)
                {
                    context.Response.StatusCode = (int)HttpStatusCode.NotFound;
                    context.Response.OutputStream.Close();
                    Console.WriteLine("unknown request {0}", context.Request.RawUrl);

                    continue;
                }

                Console.WriteLine("processing request {0}", context.Request.RawUrl);
                new Thread(worker.processRequest).Start();

            } while (true);
        }
    }
}
